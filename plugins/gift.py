# -*- coding: utf-8 -*-
from telegram.ext.dispatcher import run_async
from telegram import ParseMode

import shutil
import json
import os
import time
import random
from threading import Lock

class Gift():
    def __init__(self, bot, config):
        bot.registerCommand("gift","<registration key/link> [description] Give a game to someone",self.handleGift,True)
        bot.registerCommand("gifts","List all unclaimed gifts",self.handleGifts,True)
        bot.registerCommand("gimme","[search term] search the gift pile",self.handleGimme,True)
        bot.registerCommand("gofish","Pick a random gift from the undescribed pile",self.handleGoFish,True)
        bot.registerCommand("claim","[partial key] Claim a gift. Omit key to claim most recent search hit.",self.handleClaim,True)

        self.FILENAME = "gifts.json"
        self.lock = Lock()

        # Each gift is a dict containing
        # user: the name of the person who gave
        # descr: the optional description text
        # claimed_by: the optional name of the person who claimed this gift

        # There are three classes of gift:
        # open gifts (have a description)
        # secret gifts (have no description)
        # claimed gifts (kept in case we need to mass-revive them)

        # Each is listed as a dictionary keyed by a string,
        # where the string is either a CD-key or a URL to claim the gift.


        try:
          self.db = json.load(open("gifts.json"))
        except:
          # Unable to load the database.
          # If the file actually exists, create a backup of it so we don't
          # clobber anything important due to some weird parsing error
          if (os.path.isfile(self.FILENAME)):
            backup = "{}.{}".format(self.FILENAME,int(time.time()))
            print("GIFTS DATABASE CORRUPTED. SAVING AS {}".format(backup))
            shutil.copyfile(self.FILENAME, backup)

          self.db = {}
          self.db["open"] = {}
          self.db["secret"] = {}
          self.db["claimed"] = {}

        self.save()

        # The most recently displayed key, which is the default for /claim
        self.recent_hit = ""

    def save(self):
      with open(self.FILENAME, "w+") as f:
        json.dump(self.db, f, indent=2)

    def format(self, key, gift, shorten=False):
      msg = "secret"
      if (gift["description"] != ""):
        msg = "*{}*".format(gift["description"])

      msg += " from _{}_".format(gift["user"])

      if (not shorten):
        if ("http" in key):
          msg += ": {}".format(key)
        else:
          msg += ": `{}`".format(key)
      return msg

    # Split a multiline message into multiple messages, if needed
    def reply_lines(self, message, lines):
      N=15
      for s in range(0, len(lines), N):
        chunk = lines[s:s+N]
        msg = "\n".join(chunk)
        message.reply_text(msg, parse_mode=ParseMode.MARKDOWN, disable_web_page_preview=True)

    # Returns a list of hits.
    # Each hit is a tuple of:
    # * The kind of gift (open, secret),
    # * The full key
    # * The gift dict itself.
    # Searches open and secret gifts, ignores already claimed ones.
    def search_by_key(self, partial_key):
      # Start with perfect matches
      hits = []
      for kind in [ "open", "secret" ]:
        if partial_key in self.db[kind]:
          key = partial_key
          hits += [ (kind,key,self.db[kind][key]) ]

      # Then partial matches, if there was no exact one
      if (len(hits) == 0):
        for kind in [ "open", "secret" ]:
          for _,key in enumerate(self.db[kind]):
            if partial_key in key:
              hits += [ (kind,key,self.db[kind][key]) ]

      # If there was only one hit, give /claim a shortcut
      if (len(hits) == 1):
        _,key,_ = hits[0]
        self.recent_hit = key
      else:
        self.recent_hit = ""

      return hits

    def search_by_description(self, term):
      # Fuzzier search
      hits = []
      kind = "open"
      for _,key in enumerate(self.db[kind]):
        gift = self.db[kind][key]
        if term.lower() in gift["description"].lower():
          hits += [ (kind,key,gift) ]

      # If there was only one hit, give /claim a shortcut
      if (len(hits) == 1):
        _,key,_ = hits[0]
        self.recent_hit = key

      return hits


    @run_async
    def handleGift(self,bot,update,args):
      key = str(args[0])
      gift = {}
      gift["user"] = update.message.from_user["username"].lower()
      gift["description"] = ""
      kind = "secret"
      if (len(args) > 1):
        gift["description"] = " ".join(args[1:])
        kind = "open"


      with self.lock:
        # Key already in the database?
        for _,d in enumerate(self.db):
          if key in self.db[d]:
            msg = self.format(key, self.db[d][key])
            update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN, disable_web_page_preview=True)
            return

        self.db[kind][key] = gift
        self.save()
        update.message.reply_text("Thank you for your contribution!", parse_mode=ParseMode.MARKDOWN, disable_web_page_preview=True)


    @run_async
    def handleGifts(self,bot,update,args):
      listing = []
      with self.lock:
        for _,key in enumerate(self.db["open"]):
          listing += [ (key, self.db["open"][key]) ]

      sort_by_desc = lambda tup: tup[1]["description"]
      listing = sorted(listing, key=sort_by_desc)

      msg = [ "Open gifts:" ]
      for (key,gift) in listing:
        msg += [ self.format(key, gift, shorten=True) ]

      msg += [ "{} secret gifts (use /gofish)".format(len(self.db["secret"])) ]
      self.reply_lines(update.message, msg)


    @run_async
    def handleGimme(self,bot,update,args):
      if (len(args) == 0):
        update.message.reply_text("/gimme <search term>", parse_mode=ParseMode.MARKDOWN, disable_web_page_preview=True)
        return


      msg = []
      with self.lock:
        term = " ".join(args)
        hits = self.search_by_description(term)
        if (len(hits) == 0):
          msg = [ "No matches." ]

        elif (len(hits) == 1):
          kind, key, gift = hits[0]
          msg = [ self.format(key,gift) ]
          msg += [ "Use /claim to take it directly" ]

        else:
          msg = [ "{} matches:".format(len(hits)) ]
          for (kind, key, gift) in hits:
            msg += [ self.format(key, gift, shorten=False) ]


      self.reply_lines(update.message, msg)


    @run_async
    def handleGoFish(self,bot,update,args):
      if (len(self.db["secret"]) == 0):
        update.message.reply_text("No secret gifts in database. Try /gifts.", parse_mode=ParseMode.MARKDOWN, disable_web_page_preview=True)
        return

      with self.lock:
        key = random.choice(list(self.db["secret"].keys()))
        gift = self.db["secret"][key]
        self.recent_hit = key

        msg = self.format(key, gift)
        msg += "\nUse /claim to take it"
        update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN, disable_web_page_preview=True)

        pass


    @run_async
    def handleClaim(self,bot,update,args):
      partial_key = ""
      if (len(args) > 0):
        partial_key = args[0]
      else:
        partial_key = self.recent_hit

      if (partial_key == ""):
        update.message.reply_text("No recent search hits to claim. /claim <partial key> to specify the gift", parse_mode=ParseMode.MARKDOWN)
        return

      with self.lock:
        hits = self.search_by_key(partial_key)
        if (len(hits) == 0):
          update.message.reply_text("No matches. Are you sure that's the key for the gift?", parse_mode=ParseMode.MARKDOWN)
          return

        if (len(hits) > 1):
          msg = [ "Multiple matches:" ]
          for _,key,gift in hits:
            msg += [ self.format(key, gift, shorten=True) ]
          self.reply_lines(update.message, msg)
          return

        kind,key,gift = hits[0]
        gift["claimed_by"] = update.message.from_user["username"].lower()
        self.db["claimed"][key] = gift
        del self.db[kind][key]
        self.save()
        self.recent_hit = ""
        update.message.reply_text("Claimed " + self.format(key, gift), parse_mode=ParseMode.MARKDOWN, disable_web_page_preview=True)

mainclass = Gift
