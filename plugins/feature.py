# -*- coding: utf-8 -*-
from telegram.ext.dispatcher import run_async

class Feature():
    def __init__(self, bot, config):
        bot.registerCommand("contribute","contribute code",self.handleContribute,True)
        bot.registerCommand("feature","request feature/report bug",self.handleFeature,True)
        bot.registerCommand("features","see request(ed) feature(s)/report(ed) bug(s)",self.handleFeatures,True)

    @run_async
    def handleContribute(self,bot,update,args):
        update.message.reply_text("https://git.ludd.ltu.se/solpanda/segelbot")

    @run_async
    def handleFeature(self,bot,update,args):
        message = str(update.message["text"]).split(" ", 1)[1]
        user = str(update.message.from_user["username"])

        with open("features.txt", "a") as myfile:
            myfile.write("%s; %s \n"%(user,message))

        update.message.reply_text("> /dev/nu... eeh, Your request has been duly noted.")

    @run_async
    def handleFeatures(self,bot,update,args):
        features = ""
        with open("features.txt", "r") as myfile:
            features = myfile.read()

        update.message.reply_text(str(features))

mainclass = Feature
