import requests
import time
from telegram.ext.dispatcher import run_async

class Bocken():
    def __init__(self, bot, config):
        bot.registerCommand("bocken","Status update on gavle bocken",self.handleBocken)
    
    def get_image(self):
        head = {'referer': "http://www.visitgavle.se/sv/gavlebocken",
            'User-Agent': "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0"}
        return requests.get(("https://bocken.gavle.se/kamera/bocken.jpg?t=%d")%(int(time.time()*1000)), headers=head).content

    @run_async
    def handleBocken(self,bot,update):
        newImage = False
        with open("bocken.jpg", "wb") as f:
            ret = None
            for x in range(25):
                ret = self.get_image()
                #for some reson dont we always get a bocken
                if len(ret) > 100:
                    print("bocken found")
                    f.write(ret)
                    newImage = True
                    break
                else:
                    print("bocken not found")
                    time.sleep(.2)
        if newImage:
            update.message.reply_photo(photo = open("bocken.jpg","rb"))
        else:
            update.message.reply_text("bocken är på semester, försök igen om en stund.")


mainclass = Bocken
