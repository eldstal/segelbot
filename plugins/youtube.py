# -*- coding: utf-8 -*-
import urllib
import html.parser
import http.client
from bs4 import BeautifulSoup
from telegram.ext.dispatcher import run_async
import youtube_dl
import os

class Youtube():
    def __init__(self, bot, config):
        bot.registerCommand("dangerzone","Highway to the danger zone",self.handleDanger,True)
        bot.registerCommand("getaudio","uploads audio from youtube videos",self.handleAudio,True)

    @run_async
    def handleDanger(self,bot,update,args):
        #update.message.reply_text(message)
        update.message.reply_audio(audio=open("dangerzone.mp3", "rb"))
    
    @run_async
    def handleAudio(self,bot,update,args):
        print(args[0])

        ydl_music = {
            'format': 'bestaudio/best',
            'outtmpl': '%(title)s.%(ext)s',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '192',
            }]#,
            #'logger': MyLogger(),
            #'progress_hooks': [my_hook],
            }
        with youtube_dl.YoutubeDL(ydl_music) as ydl:
            data = ydl.extract_info(str(args[0]),
                                    download = False)
            if data["is_live"]:
                update.message.reply_text("no live video")
                return
            if data["duration"] > 1800:
                update.message.reply_text("to long video")
                return
            ydl.extract_info(str(args[0]),
                            download = True)
            print(data["title"])
            fname = ydl.prepare_filename(data).rsplit(".",1)[0]+".mp3"
            update.message.reply_audio(audio=open(fname,"rb"))
            os.remove(fname)	


mainclass = Youtube
