# -*- coding: utf-8 -*-
# Most of this code shamelessly ripped from D0009E
# https://github.com/Belgarion/D0009E/
from telegram.ext.dispatcher import run_async

import random
import codecs
from hyphenator import Hyphenator
from telegram import ParseMode

class Haiku():
    def __init__(self, bot, config):
        self.definitions = []
        self.substantiv = []
        self.adjektiv = []
        self.adverb = []
        self.verb = []
        self.namn = []
        self.prepositioner = []
        self.hyph = Hyphenator("dict/hyph_sv_SE.dic")

        self.loadWords()
        bot.registerCommand("haiku","Generate a beautiful poem",self.handleHaiku,True)

    @run_async
    def handleHaiku(self,bot,update,args):
        msg = "~\n"
        msg += "_" + self.generateHaikuSentence(5,50) + "_\n"
        msg += "_" + self.generateHaikuSentence(7,50) + "_\n"
        msg += "_" + self.generateHaikuSentence(5,50) + "_\n"
        update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)



    def loadWords(self):
        f = codecs.open("dict/dsso-1.44.txt",'r','utf-8')

        for line in f.readlines():
            if line.split(":")[0] == "DEFINITION 1":
                self.definitions.append([line.split(":")[1].lower()])

            line.replace("\n","")
            line.replace("\r","")
            if ">" in line:
                wordType = line.split("<")[1].split(">")[0]
                word = line.split(">")[1].split(":")
                if wordType == "substantiv":
                    self.substantiv.append(word)
                elif wordType == "adjektiv":
                    self.adjektiv.append(word)
                elif wordType == "adverb":
                    self.adverb.append(word)
                #elif wordType == "pronomen":
                #    self.pronomen.append(word)
                elif wordType == "verb":
                    self.verb.append(word)
                elif wordType  == "egennamn":
                    self.namn.append(word)
                elif wordType  == "preposition":
                    self.prepositioner.append(word)

        f.close()


    def generateHaikuSentence(self, length, maxtries):
        sentence = []
        while length > 0 or maxtries > 0:
            wordtype = random.randint(0,99)
            if wordtype >= 0 and wordtype <25:
                o = random.choice(self.substantiv)[0]
            elif wordtype >= 25 and wordtype <50:
                o = random.choice(self.verb)[0]
            elif wordtype >= 50 and wordtype <75:
                o = random.choice(self.adjektiv)[0]
            elif wordtype >=75 and wordtype <99:
                o = random.choice(self.prepositioner)[0]
            hyphword = self.hyph.inserted(o)
            hyphlist = hyphword.split("-")
            syllableCount = len(hyphlist)
            if syllableCount <= length:
                sentence.append("".join(hyphlist))
                length-=syllableCount
            maxtries-=1
        random.shuffle(sentence)
        return " ".join(sentence)

mainclass = Haiku
