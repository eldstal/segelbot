# -*- coding: utf-8 -*-
import urllib
import json
import html.parser
import http.client
from telegram.ext.dispatcher import run_async

class Down():
    def __init__(self, bot, config):
        bot.registerCommand("down","<dns> http(s) only",self.handleDown,True)
    @run_async
    def handleDown(self,bot,update,args):
        try:
            conn = http.client.HTTPSConnection("api.downfor.cloud")
            conn.request("GET", "/httpcheck/%s" % args[0])
            if json.loads(conn.getresponse().read().decode('utf-8'))["isDown"]:
                message = "%s is down" % args[0]
            else:
                message = "%s is up" % args[0]
        except urllib.error.URLError:
            message = "No result. https://downforeveryoneorjustme.com is down."
        except IndexError:
            message = "seams like you inputed an inavlid url"
        update.message.reply_text(message)
mainclass = Down
