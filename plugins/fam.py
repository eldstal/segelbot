# -*- coding: utf-8 -*-
import urllib
import html.parser
import http.client

class Fam():
    def __init__(self, bot, config):
        bot.registerCommand("fam","markov chain from familjeliv comments",self.handleFam,True)
    def handleFam(self, bot, update, args):
        try:
            message = ""
            conn = http.client.HTTPConnection("nkrs.nu:8080")
            conn.request("GET","/")
            message = conn.getresponse().read().decode('utf-8')
            if len(message) < 1:
                message = "No data sent from nkrs.nu:8080"
        except urllib.error.URLError:
            message = "No result. http://nkrs.nu:8080 seams to be down"
        update.message.reply_text(message)
mainclass = Fam
