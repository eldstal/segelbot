import http.client
import requests
from telegram.ext.dispatcher import run_async

class Inspirobot():
    def __init__(self, bot, config):
        bot.registerCommand("motivate","gives you motivation",self.handleInspirobot)

    def get_image_link(self):
        r = requests.get("https://inspirobot.me/api?generate=true")
        return r.text

    @run_async
    def handleInspirobot(self,bot,update):
        update.message.reply_text(self.get_image_link())

mainclass = Inspirobot
