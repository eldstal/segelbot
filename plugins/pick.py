# -*- coding: utf-8 -*-
from telegram.ext.dispatcher import run_async
import requests
import giphy_client
import random
from time import strftime
import datetime

class Pick():
    def __init__(self, bot, config):
        try:
          self.giphy_key = config["giphyapikey"]
          self.giphy = giphy_client.DefaultApi()
        except KeyError as e:
          print("No GIPHY API key specified.")

        bot.registerCommand("pick","<option> <option> ... Pick one of the options",self.handlePick,True)
        bot.registerCommand("verdict","/pick for a simple yes/no dilemma",self.handleVerdict,True)
        bot.registerCommand("friday","/friday for FRIDAY!",self.handleFriday,True)

    @run_async
    def handlePick(self,bot,update,args):
        if "," in " ".join(args):
        # check if arg is csv
            args = [x.strip() for x in list(filter(None," ".join(args).split(",")))]
        if len(args) < 2:
        # Cheeky
            self.doPick(bot, update, [ "Try again" ])
        else:
            self.doPick(bot, update, args)

    @run_async
    def handleVerdict(self,bot,update,args):
        self.doPick(bot, update, ["yes", "no", "never", "absolutely", "totally", "no way" ])

    @run_async
    def handleFriday(self,bot,update,args):
        wday = strftime("%w")
        if wday == "5" or self.isTomorrowHoliday() == True:
            self.doPick(bot, update, ["friday","fredag","freitag","weekend","viernes"])
        else:
            self.doPick(bot, update, ['nope', 'not yet', 'wait', 'not today'])

    def isTomorrowHoliday(self):
        date = (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%Y/%m/%d")
        data = requests.get("https://api.dryg.net/dagar/v2.1/%s" % date).json()
        print(data)
        if data["dagar"][0]["arbetsfri dag"] == "Ja" and data["dagar"][0]["dag i vecka"] != 7:
            print("röd dag")
            return True
        print("inte röd")
        return False

    def doPick(self, bot, update, options):
      term = random.choice(options)
      gif_url = self.getRandomGif(term)

      if (gif_url is None):
        # Fallback to text response
        update.message.reply_text(term)
      else:
        update.message.reply_animation(gif_url)
      pass

    def getRandomGif(self, term):
      if (self.giphy_key is None):
        return None

      try:
        response = self.giphy.gifs_search_get(self.giphy_key, term, limit=25, offset=0, lang="en", fmt="json")
        picked = random.choice(response.data).images.original.url
        return picked
      except:
        return None
      return None

mainclass = Pick
