# -*- coding: utf-8 -*-

import json
from telegram.ext.dispatcher import run_async
import geocoder
import requests

def ifthen(key,dic,default):
    return dic[key] if key in dic else default

def cardinal(degrees):
    names = ["N","NE","E","SE","S","SW", "W", "NW"]
    degrees_per_direction = 360/len(names)
    arc = degrees_per_direction / 2
    # shift zero so that it sits between NW and N
    return names[int((degrees + arc) // degrees_per_direction) % len(names)]

class Temp():
    def __init__(self, bot, config):
        bot.registerCommand("temp","<location>",self.handleTemp,True)
        bot.registerCommand("location","<location>",self.handleLocation,True)
        self.apikey = config["openweatherapikey"]
        try:
            with open("locations.json", "r") as fp:
                self.locations = json.load(fp)
        except FileNotFoundError:
            self.locations = {}

    @run_async
    def handleLocation(self,bot,update,args):
        print(args)
        if args != []:
            if args[0] == "remove" or args[0] == "campus":
                del self.locations[str(update.message.from_user["username"]).lower()]
            else:
                self.locations[str(update.message.from_user["username"]).lower()] = " ".join(args)
            with open("locations.json","w") as fp:
                json.dump(self.locations, fp, sort_keys = True, indent=4)
        else:
            update.message.reply_text("useage '/location remove' or '/location <place in real world>")

    @run_async
    def handleTemp(self, bot, update, args):
        username = str(update.message.from_user["username"]).lower()
        if args == [] and username in self.locations:
            targetLocation = self.locations[username]
        elif args == []:
            targetLocation = ""
        elif args[0].lower() in self.locations:
            targetLocation = self.locations[args[0].lower()]
        else:
            targetLocation = " ".join(args)

        location = geocoder.osm(targetLocation).osm
        print(location)
        if location == None and targetLocation == "":
            d = requests.get("https://temp.campus.ltu.se/temp_outside.php").text
            update.message.reply_text("Temperature in Luleå (campus): %s degrees Celsius" % d)
            return

        if location == None:
            update.message.reply_text("%s cound not be found"% args)
            return

        if location["addr:country"] == "Sverige":
            weatherdata = self.smhi(location["y"], location["x"])
            description = self.weatherid(weatherdata[3])
        else:
            weatherdata = self.openweather(location["y"], location["x"])
            description = weatherdata[3]
        
        winddir = weatherdata[1]
        windspd = weatherdata[2]
        if windspd != -9.0:
            windstr = "%.1f m/s %s, " % (windspd, cardinal(winddir))
        else:
            windstr = ""

        weatherstring = "{temp:.1f} °C, {wind}{desc} @ {place}, {area}, {country}".format(
             temp = weatherdata[0],
             wind = windstr,
             desc = description,
             place = location.get('addr:city', targetLocation.split(" ")[0]),
             area = location.get('addr:postal', location.get('addr:state', "Unknown")),
             country = location["addr:country"],
        )
        update.message.reply_text(weatherstring)
        print(weatherdata)
        return None

    def smhi(self, lat, lon):
        d = requests.get(("https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/%f/lat/%f/data.json")%(lon,lat)).json()
        return (d["timeSeries"][0]["parameters"][11]["values"][0],
                d["timeSeries"][0]["parameters"][13]["values"][0],
                d["timeSeries"][0]["parameters"][14]["values"][0],
                d["timeSeries"][0]["parameters"][18]["values"][0])

    def openweather(self, lat, lon):
        d = requests.get("https://api.openweathermap.org/data/2.5/weather?units=metric&lat=%f&lon=%f&APPID=%s" % (lat, lon, self.apikey)).json()
        print(d)
        return (d["main"]["temp"],
                d["wind"]["deg"],
                d["wind"]["speed"],
                d["weather"][0]["description"])

    def weatherid(self, id):
        return {
        1: "Clear sky",
        2: "Nearly clear sky",
        3: "Variable cloudiness",
        4: "Halfclear sky",
        5: "Cloudy sky",
        6: "Overcast",
        7: "Fog",
        8: "Light rain showers",
        9: "Moderate rain showers",
        10: "Heavy rain showers",
        11: "Thunderstorm",
        12: "Light sleet showers",
        13: "Moderate sleet showers",
        14: "Heavy sleet showers",
        15: "Light snow showers",
        16: "Moderate snow showers",
        17: "Heavy snow showers",
        18: "Light rain",
        19: "Moderate rain",
        20: "Heavy rain",
        21: "Thunder",
        22: "Light sleet",
        23: "Moderate sleet",
        24: "Heavy sleet",
        25: "Light snowfall",
        26: "Moderate snowfall",
        27: "Heavy snowfall"
        }.get(id, "Description unavailable")

mainclass = Temp
